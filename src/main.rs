use std::process::Command;

fn main() {
    Command::new("agetty")
        .arg("tty1")
        .spawn()
        .expect("agetty did not start.");
    loop {
        // Forever
    }
}
